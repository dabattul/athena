/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/JaggedVecImpl.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Jul, 2024
 * @brief Definition of JaggedVecElt.
 */


namespace SG {


/**
 * @brief Constructor.
 * @param end Index of the end of the range.
 */
inline
JaggedVecEltBase::JaggedVecEltBase (index_type end)
  : m_end (end)
{
}


/**
 * @brief Return the index of the beginning of the range.
 * @param elt_ndx The index of this element in its container.
 */
inline
JaggedVecEltBase::index_type JaggedVecEltBase::begin (size_t elt_ndx) const
{
  // If the index is more than zero, return the end index from the
  // element which is contiguously previous in memory.
  if (elt_ndx > 0) {
    return this[-1].m_end;
  }
  // Otherwise this is the first element.  Return 0.
  return 0;
}


/**
 * @brief Return the index of the end of the range.
 */
inline
JaggedVecEltBase::index_type JaggedVecEltBase::end() const
{
  return m_end;
}


/**
 * @brief Return the number of items in this range.
 * @param elt_ndx The index of this element in its container.
 */
inline
size_t JaggedVecEltBase::size (size_t elt_ndx) const
{
  return end() - begin (elt_ndx);
}


/**
 * @brief Equality test.
 * @param other Other element with which to compare.
 */
inline
bool JaggedVecEltBase::operator== (const JaggedVecEltBase& other) const
{
  return m_end == other.m_end;
}


/// Constructor.
inline
JaggedVecEltBase::Shift::Shift (int offs)
  : m_offs (offs)
{
}


/// Shift indices in @c e by the offset given to the constructor.
inline
void JaggedVecEltBase::Shift::operator() (JaggedVecEltBase& e) const
{
  e.m_end += m_offs;
}



} // namespace SG
