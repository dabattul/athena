/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/MeasurementToTrackParticleDecoration.h"

DECLARE_COMPONENT(ActsTrk::MeasurementToTrackParticleDecoration)
