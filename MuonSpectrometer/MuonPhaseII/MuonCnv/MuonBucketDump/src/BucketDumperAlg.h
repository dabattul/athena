#ifndef MUONCSVDUMP_BucketDumperAlg_H
#define MUONCSVDUMP_BucketDumperAlg_H
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include <AthenaBaseComps/AthAlgorithm.h>
#include "AthenaBaseComps/AthHistogramAlgorithm.h"

#include <MuonIdHelpers/IMuonIdHelperSvc.h>
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadDecorHandle.h"
#include "StoreGate/ReadHandleKeyArray.h"
#include "StoreGate/ReadCondHandleKey.h"

#include <MuonPatternEvent/MuonPatternContainer.h>
#include <MuonSpacePoint/SpacePointContainer.h>
#include <ActsGeometryInterfaces/ActsGeometryContext.h>
#include <MuonReadoutGeometryR4/MuonDetectorManager.h>

#include "MuonTesterTree/MuonTesterTree.h"
#include "MuonTesterTree/ThreeVectorBranch.h"
#include "MuonTesterTree/IdentifierBranch.h"

#include "xAODMuonSimHit/MuonSimHitContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"

#include "AthenaKernel/IAthRNGSvc.h"
#include "CLHEP/Random/RandomEngine.h"


namespace MuonR4{
class BucketDumperAlg: public AthHistogramAlgorithm {

   public:
    using AthHistogramAlgorithm::AthHistogramAlgorithm;
    ~BucketDumperAlg() = default;

    virtual StatusCode initialize() override final;
    virtual StatusCode finalize() override final;
    virtual StatusCode execute() override final;

   private:

    void fillChamberInfo(const MuonGMR4::SpectrometerSector* chamber); 

    SG::ReadHandleKey<SpacePointContainer> m_readKey{this, "ReadKey", "MuonSpacePoints", "Key to the space point container"};
    ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc{this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};

    SG::ReadHandleKeyArray<xAOD::MuonSimHitContainer> m_inSimHitKeys {this, "SimHitKeys",{}, "xAOD  SimHit collections"};
    SG::ReadHandleKey<MuonR4::SegmentContainer> m_inSegmentKey{this, "SegmentKey", "R4MuonSegments"};

    Gaudi::Property<bool> m_isMC{this, "isMC", true};
    Gaudi::Property<double> m_fracToKeep{this,"dataFracToKeep", 0.055};
    Gaudi::Property<std::string> m_streamName{this, "StreamName", ""};
    ServiceHandle<IAthRNGSvc> m_rndmSvc{this, "RndmSvc", "AthRNGSvc", ""};
   CLHEP::HepRandomEngine* getRandomEngine(const EventContext&ctx) const;

    MuonVal::MuonTesterTree m_tree{"MuonBucketDump","MuonBucketDump"};

    MuonVal::ScalarBranch<float>&           m_bucket_min{m_tree.newScalar<float>("bucket_min", -1)};
    MuonVal::ScalarBranch<float>&           m_bucket_max{m_tree.newScalar<float>("bucket_max", -1)};
    MuonVal::ScalarBranch<uint16_t>&        m_bucket_spacePoints{m_tree.newScalar<uint16_t>("bucket_spacePoints", 0)};
    MuonVal::ScalarBranch<uint16_t>&        m_bucket_segments{m_tree.newScalar<uint16_t>("bucket_segments", 0)};

    MuonVal::MuonIdentifierBranch           m_spoint_id{m_tree, "id"};
    MuonVal::ThreeVectorBranch              m_spoint_localPosition{m_tree, "localPosition"}; 
    MuonVal::VectorBranch<uint16_t>&        m_spoint_layer{m_tree.newVector<uint16_t>("Layer")};
    MuonVal::VectorBranch<bool>&            m_spoint_isMdt{m_tree.newVector<bool>("isMdt", false)};
    MuonVal::VectorBranch<bool>&            m_spoint_isStrip{m_tree.newVector<bool>("isStrip", false)};

    MuonVal::VectorBranch<uint16_t>&        m_spoint_adc{m_tree.newVector<uint16_t>("adc")};

    MuonVal::VectorBranch<float>&           m_spoint_covX{m_tree.newVector<float>("covX")};
    MuonVal::VectorBranch<float>&           m_spoint_covXY{m_tree.newVector<float>("covXY")};
    MuonVal::VectorBranch<float>&           m_spoint_covYX{m_tree.newVector<float>("covYX")};
    MuonVal::VectorBranch<float>&           m_spoint_covY{m_tree.newVector<float>("covY")};
    MuonVal::VectorBranch<float>&           m_spoint_driftR{m_tree.newVector<float>("driftR")};

    MuonVal::VectorBranch<bool>&            m_spoint_measuresEta{m_tree.newVector<bool>("measuresEta")};
    MuonVal::VectorBranch<bool>&            m_spoint_measuresPhi{m_tree.newVector<bool>("measuresPhi")};
    MuonVal::VectorBranch<unsigned int>&    m_spoint_nEtaInstances{m_tree.newVector<unsigned int>("nEtaInUse")};
    MuonVal::VectorBranch<unsigned int>&    m_spoint_nPhiInstances{m_tree.newVector<unsigned int>("nPhiInUse")};
    MuonVal::VectorBranch<unsigned int>&    m_spoint_dimension{m_tree.newVector<unsigned int>("dimension")};

    MuonVal::VectorBranch<uint16_t>&        m_spoint_nSegments{m_tree.newVector<uint16_t>("nSegments")};
    MuonVal::MatrixBranch<int16_t>&         m_spoint_mat{m_tree.newMatrix<int16_t>("sp_seg_matching",-1)};
    MuonVal::ThreeVectorBranch              m_segmentPos{m_tree, "segmentPosition"}; 
    MuonVal::ThreeVectorBranch              m_segmentDir{m_tree, "segmentDirection"};
    MuonVal::VectorBranch<float>&           m_segment_chiSquared{m_tree.newVector<float>("segment_chiSquared")};
    MuonVal::VectorBranch<float>&           m_segment_numberDoF{m_tree.newVector<float>("segment_numberDoF")};

    size_t m_event{0};

};
}
#endif
