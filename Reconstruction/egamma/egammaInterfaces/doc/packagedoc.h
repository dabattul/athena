/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**

@page egammaInterfaces_page egammaInterfaces Package

This package is provides interfaces for
egamma reconstruction tools


*/
